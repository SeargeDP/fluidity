package the_fireplace.fluidity.entity.tile;

import the_fireplace.fluidity.enums.FluidityIronChestType;

public class TileEntityElectrumChest extends TileEntityFluidityIronChest {
	public TileEntityElectrumChest()
	{
		super(FluidityIronChestType.ELECTRUM);
	}
}
