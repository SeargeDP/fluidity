package the_fireplace.fluidity.entity.tile;

import the_fireplace.fluidity.enums.FluidityIronChestType;

public class TileEntityNickelChest extends TileEntityFluidityIronChest {
	public TileEntityNickelChest()
	{
		super(FluidityIronChestType.NICKEL);
	}
}
