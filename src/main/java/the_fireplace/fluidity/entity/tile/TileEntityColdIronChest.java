package the_fireplace.fluidity.entity.tile;

import the_fireplace.fluidity.enums.FluidityIronChestType;

public class TileEntityColdIronChest extends TileEntityFluidityIronChest {
	public TileEntityColdIronChest()
	{
		super(FluidityIronChestType.COLDIRON);
	}
}
