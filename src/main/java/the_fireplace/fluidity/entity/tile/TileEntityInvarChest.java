package the_fireplace.fluidity.entity.tile;

import the_fireplace.fluidity.enums.FluidityIronChestType;

public class TileEntityInvarChest extends TileEntityFluidityIronChest {
	public TileEntityInvarChest()
	{
		super(FluidityIronChestType.INVAR);
	}
}
