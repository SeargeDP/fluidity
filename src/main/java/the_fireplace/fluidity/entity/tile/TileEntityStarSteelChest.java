package the_fireplace.fluidity.entity.tile;

import the_fireplace.fluidity.enums.FluidityIronChestType;

public class TileEntityStarSteelChest extends TileEntityFluidityIronChest {
	public TileEntityStarSteelChest()
	{
		super(FluidityIronChestType.STARSTEEL);
	}
}
