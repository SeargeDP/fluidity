package the_fireplace.fluidity.entity.tile;

import the_fireplace.fluidity.enums.FluidityIronChestType;

public class TileEntityAdamantineChest extends TileEntityFluidityIronChest {
	public TileEntityAdamantineChest()
	{
		super(FluidityIronChestType.ADAMANTINE);
	}
}
